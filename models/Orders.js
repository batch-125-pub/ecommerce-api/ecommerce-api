let mongoose = require('mongoose');

let orderSchema = new mongoose.Schema(
	{
		totalAmount: { //should compute the sum of the products added when an order is created (updated everytime a product is added in an order)
			type: Number
		},
		paymentStatus: { // status will only change when user checks out or pays
			type: Boolean,
			default: false 
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		userId: { //pass on the id of the current user
			type: String,
			required: [true, "userId is required"]
		},
		userFirstName: {
			type: String,
			required: [true, "first name is required"]
		},
		userLastName: {
			type: String,
			required: [true, "last name is required"]
		},
		cartCount: {
			type: Number,
			default: 0
		},
		products: [
				{
					productId: {
						type: String
					},
					name: {
						type: String
					},
					price: {
						type: Number
					},
					quantity: {
						type: Number
					},
					subtotal: {
						type: Number
					}

			}
		]
	}	
);

module.exports = mongoose.model("Order", orderSchema);