let mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Order Name is required"]
		},
		description: {
			type: String,
			required: [true, "Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		imgURL: {
			type: String
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}	
	}
);

module.exports = mongoose.model("Product", productSchema);