let mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
	{	
		firstName: {
			type: String
		},
		lastName: {
			type: String
		},
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		password: {
			type: String,
			required: [true, "Password is required"]
		},
		isAdmin: {
			type: String,
			default: false
		},
	}

);

module.exports = mongoose.model("User", userSchema);