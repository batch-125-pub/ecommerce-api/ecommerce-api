let jwt = require('jsonwebtoken');

let secret = "ecommerceAPI";

module.exports.createAccessToken = (user) => {
	let data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"});
			} else {
				next();
			}
		});
	}
}

module.exports.decode = (token) => {
	//console.log("token from decode", token);
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.decode(token, {complete: true}).payload;
	}
}

