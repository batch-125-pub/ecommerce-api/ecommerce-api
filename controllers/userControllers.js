
let User = require('./../models/Users');

let bcrypt = require('bcrypt');

let auth = require('./../auth');

module.exports.checkEmail = (reqBody) => {
	let returnVal = User.findOne({email: reqBody.email}).then((result)=> {
		console.log(returnVal);
		if(result){
			return true; //return true if checked email is found (user with email is already existing in the system); tell front end the the email is already existing and it cannot be used anymore
		} else {
			return false; //return false if checked email is not found (email checked is unique and can be newly registered); tell frontend that the email is unique.
		}
	})

	return returnVal;
	
}

module.exports.register = (reqBody) => {

	let createUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return createUser.save().then((result, error)=>{
		if(error){
			return error;
		} else {
			//console.log("result", result);
			return true;
		}
	})
}

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result)=> {
		console.log("after find one", result);
		if(result == null){
			return false; //user fails to log in
		} else {

			console.log("in login", result);
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect === true){
				console.log({access: auth.createAccessToken(result.toObject())});
				return {access: auth.createAccessToken(result.toObject())};
			} else {
				return false; //password is not correct; tell front end that the user needs to re-login or user failed to login.
			}

		}

	})
}

module.exports.getProfile = (user) => {
	return User.findOne({_id: user.id}).then(result=>{return result});
}

module.exports.getAllUsers = () => {
	return User.find({}).then(result=>{return result}); //in frontend, add href to update isAdmin (add a checker controller / get fetch first to check if user is already an admin (if yes, alert that user is already an admin))
}

module.exports.getAdminStatus = (passedUser) => {
	return User.findOne({_id: passedUser}).then(result=>{return result.isAdmin}); //will pass the admin status of the selected user
}

module.exports.setToAdmin = (passedUser) => {
	let newUpdate = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(passedUser, newUpdate, {new: true}).then((result, error)=>{
		if(error){
			return error; 
		} else {
			return true; //user was set to an Admin
		}
	})
}



