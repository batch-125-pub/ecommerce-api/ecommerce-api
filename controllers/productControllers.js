let Product = require("./../models/Products");

let auth = require("./../auth");

module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		imgURL: reqBody.imgURL
	})

	return newProduct.save().then((result, error)=>{
		if(error){
			return error;
		} else {
			return true; // will return true if the product was successfully saved;
		}
	})
}

module.exports.getAllActiveProd = () => {
	return Product.find({isActive: true}).then((result)=>{return result});
}

module.exports.getAllProducts = () => {
	return Product.find({}).then((result)=>{return result});
}

module.exports.getSingleProduct = (params) => {
	return Product.findById(params).then(result=>{return result});
}

module.exports.editProduct = (params, reqBody) => {

	console.log("in edit product");
	let newUpdate = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		imgURL: reqBody.imgURL
	}

	return Product.findByIdAndUpdate(params, newUpdate, {new: true}).then((result, error)=>{

		if(error){
			return error; 
		} else {
			return true; //product was successfully updated
		}
	})
}

module.exports.deleteProduct = (params) => {

	return Product.findByIdAndDelete(params).then((result, error)=>{
		if(error){
			return error; //product is not deleted
		} else {
			return true; //product was successfully deleted
		}
	})
}

module.exports.archive = (params) => {
	return Product.findByIdAndUpdate(params, {isActive: false}, {new: true}).then((result, error)=>{
		if(error){
			return error;
		} else {
			return true; //product was successfully archived
		}
	})
}

module.exports.unarchive = (params) => {
	return Product.findByIdAndUpdate(params, {isActive: true}, {new: true}).then((result, error)=>{
		if(error){
			return error;
		} else {
			return true; //product was successfully unarchived
		}
	})
}


